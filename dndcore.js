/*function allowDrop(event){
	event.preventDefault();
}

function drag(event){
	event.dataTransfer.setData("text", event.target.id);
}

function drop(event){
	event.preventDefault();
	var data = event.dataTransfer.getData("text");
	event.target.appendChild(document.getElementById(data));
}*/



//document.addEventListener("DOMContentLoaded", OnLoad());

/*function OnLoad()
{
	  var ball = document.getElementById('ball');
	  ball.ondragstart = function() { return false; }

    ball.onmousedown = function(event) {
    	var coords = getCoords(ball);
    	var shiftX = event.pageX - coords.left;
    	var shiftY = event.pageY - coords.top;

    	if(ball.style.zIndex == "")
    	{
    		ball.style.position = 'absolute';
    		ball.style.zIndex = 1000;
    	}
    	
    	function moveAt(event) {
			  ball.style.left = event.pageX - shiftX + 'px';
    		ball.style.top = event.pageY - shiftY + 'px';
		  }
      	
      moveAt(event);
      document.body.appendChild(ball);

      document.onmousemove = function(event) {
        moveAt(event);
      };

      ball.onmouseup = function() {
        document.onmousemove = null;
        ball.onmouseup = null;
      };
    }
}*/


function getCoords(target){
	var box = target.getBoundingClientRect();
	return {
		top: box.top + pageYOffset,
		left: box.left + pageXOffset
	};
}

function checkCollision(boundArray)
{
  for(var i = 0; i < boundArray.length; i++)
  {
    var obj = document.elementFromPoint(boundArray[i][0], boundArray[i][1]);
    if(obj.closest(".draggable"))
    {
      alert("Collision");
      return obj;
    }
      
  }
  return null;
}




var DragManager = new function() {
  var dragObject = {};
  var self = this;

  function onMouseDown(event) {
    if(event.which != 1) return;    //if not left click

    var element = event.target.closest(".draggable");
    if(!element) return;

    dragObject.elem = element;        //save elem and click coords
    dragObject.downX = event.pageX;
    dragObject.downY = event.pageY;

    return false;
  };

  function onMouseMove(event) {
    if(!dragObject.elem) return;      //if no mousedown event

    if(!dragObject.avatar)            //if move didn't start
    {
      var moveX = event.pageX - dragObject.downX;
      var moveY = event.pageY - dragObject.downY;

      if(Math.abs(moveX) < 3 && Math.abs(moveY) < 3) return;    //low shift

      dragObject.avatar = createAvatar(event);
      if(!dragObject.avatar)
      {
        dragObject = {};
        return;
      }
      else
      {
        var coords = getCoords(dragObject.avatar);
        dragObject.shiftX = dragObject.downX - coords.left;
        dragObject.shiftY = dragObject.downY - coords.top;
        startDrag(event);
      }
    }

    dragObject.avatar.style.left = event.pageX - dragObject.shiftX + "px";
    dragObject.avatar.style.top = event.pageY - dragObject.shiftY + "px";

    return false;
  };

  function onMouseUp(event) {
    if(dragObject.avatar)       //if not single click
      finishDrag(event);

    dragObject = {};
  };

  function finishDrag(event) {
    //need check element position
    //findDroppable()
    //self.onDragEnd(dragObject);

    var dropElem = findDraggable(event);
    if(dropElem) self.onDragCancel(dragObject);
    else self.onDragEnd(dragObject);
  };

  function createAvatar(event) {
    var avatar = dragObject.elem;
    var oldStatement = {
      parent: avatar.parentNode,
      nextSibling: avatar.nextSibling,
      position: avatar.style.position || '',
      left: avatar.style.left || '',
      top: avatar.style.top || '',
      zIndex: avatar.style.zIndex || ''
    };

    avatar.rollback = function() {
      oldStatement.parent.insertBefore(avatar, oldStatement.nextSibling);
      alert("rollback");
      avatar.style.position = oldStatement.position;
      avatar.style.left = oldStatement.left;
      avatar.style.top = oldStatement.top;
      avatar.style.zIndex = oldStatement.zIndex;
    };

    return avatar;
  };

  function startDrag(event) {
    var avatar = dragObject.avatar;

    document.body.appendChild(avatar);
    avatar.style.zIndex = 9999;
    avatar.style.position = "absolute";
  };

  function findDraggable() {
    dragObject.avatar.hidden = true;

    var elem = document.elementFromPoint(event.clientX, event.clientY);
    //some problems with borders
    if(!elem.closest(".draggable"))
    {
      dragObject.avatar.hidden = false;
      var bound = dragObject.avatar.getBoundingClientRect();
      dragObject.avatar.hidden = true;
      var boundArray = [[bound.left, bound.top], [bound.left, bound.bottom], [bound.right, bound.top], [bound.right, bound.bottom]];
      elem = checkCollision(boundArray);
    }

    dragObject.avatar.hidden = false;
    if(elem) return elem.closest(".draggable");
    else return null;
  }

  document.onmousemove = onMouseMove;
  document.onmouseup = onMouseUp;
  document.onmousedown = onMouseDown;

  //this.onDragEnd = function(dragObject, dropElem) {};
  this.onDragEnd = function(dragObject) {};
  this.onDragCancel = function(dragObject) {
    dragObject.avatar.rollback();
  };
}